## marlin-user 10 QP1A.191005.007.A3 5972272 release-keys
- Manufacturer: google
- Platform: msm8996
- Codename: marlin
- Brand: google
- Flavor: marlin-user
- Release Version: 10
- Id: QP1A.191005.007.A3
- Incremental: 5972272
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/marlin/marlin:10/QP1A.191005.007.A3/5972272:user/release-keys
- OTA version: 
- Branch: marlin-user-10-QP1A.191005.007.A3-5972272-release-keys
- Repo: google_marlin_dump_31758


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
