#!/bin/bash

cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreQt.apk.* 2>/dev/null >> system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreQt.apk
rm -f system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreQt.apk.* 2>/dev/null
cat system/system/product/app/Photos/Photos.apk.* 2>/dev/null >> system/system/product/app/Photos/Photos.apk
rm -f system/system/product/app/Photos/Photos.apk.* 2>/dev/null
cat system/system/product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk.* 2>/dev/null >> system/system/product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk
rm -f system/system/product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk.* 2>/dev/null
cat system/system/product/app/GoogleCamera/GoogleCamera.apk.* 2>/dev/null >> system/system/product/app/GoogleCamera/GoogleCamera.apk
rm -f system/system/product/app/GoogleCamera/GoogleCamera.apk.* 2>/dev/null
cat system/system/product/app/WallpapersBReel/WallpapersBReel.apk.* 2>/dev/null >> system/system/product/app/WallpapersBReel/WallpapersBReel.apk
rm -f system/system/product/app/WallpapersBReel/WallpapersBReel.apk.* 2>/dev/null
